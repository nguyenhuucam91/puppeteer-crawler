module.exports = {

  pinterest: {
    hostname: ["in.pinterest.com", "www.pinterest.com"]
  },

  facebook: {
    hostname: ["www.facebook.com", "m.facebook.com"]
  },

  telegram: {
    hostname: "t.me"
  },

  tiktok: {
    hostname: "www.tiktok.com"
  },

  youtube: {
    hostname: ["www.youtube.com", "www.m.youtube.com"]
  }
}
