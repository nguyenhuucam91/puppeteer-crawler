# Tool to crawl data from social network

## Installation

Run `npm install` to install packages

## Running

Run `npm run start` to start application. Application will be booted up on port `3000`

## Supported hosts

- Facebook
- Pinterest
- Telegram
- Youtube (without video)

## Api endpoint + Parameters

POST `/crawl`

### Body

- `url`: Original url of social network post. Example: `https://www.facebook.com/tintucvtv24/posts/1544212119099904&width=500`

### Returns

Json data from crawling
