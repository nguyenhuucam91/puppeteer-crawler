var facebookPuppeteer = require('../../../puppeteer/facebook/app')
var pinterestPuppeteer = require('../../../puppeteer/pinterest/app')
var telegramPuppeteer = require('../../../puppeteer/telegram/app')
// var tiktokPuppeteer = require('../../../puppeteer/tiktok/app')
var youtubePuppeteer = require('../../../puppeteer/youtube/app')
var store = require('store')
var urlParser = require('url')
var host = require('../../../../config/hosts')
// const embedGenerator = require('../../../services/embed-generator')

module.exports = {
  show: async function (req, res) {

    if (req.method == "POST") {
      const {
        url
      } = req.body;

      const urlPath = `${req.protocol}://${req.headers.host}/crawl`
      const hostnameFormBody = urlParser.parse(url).hostname

      let embededContentJson = null;
      let isRequestSuccess = false

      if (host.facebook.hostname.includes(hostnameFormBody)) {
        generatedEmbedCode = `<iframe
        src = "https://www.facebook.com/plugins/post.php?href=${url}&show_text=1&width=500"
        width="500" height="759" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"
        allow="encrypted-media"></iframe>`
        store.set('embedded', generatedEmbedCode)
        embededContentJson = await facebookPuppeteer(urlPath);
        isRequestSuccess = true
      }

      if (host.pinterest.hostname.includes(hostnameFormBody)) {
        generatedEmbedCode = `<a data-pin-do="embedPin" href="${url}"></a>
        <script async defer src="https://assets.pinterest.com/js/pinit.js"></script>`
        store.set('embedded', generatedEmbedCode)
        embededContentJson = await pinterestPuppeteer(urlPath)
        isRequestSuccess = true
      }

      if (hostnameFormBody == host.telegram.hostname) {
        const simpleUrl = url.match(/https:\/\/t.me\/((\w+)\/(\d+))/)[1]
        generatedEmbedCode = `<script async src="https://telegram.org/js/telegram-widget.js?8" data-telegram-post="${simpleUrl}"
        data-width="100%" data-userpic="true"></script>`
        store.set('embedded', generatedEmbedCode)
        embededContentJson = await telegramPuppeteer(urlPath)
        isRequestSuccess = true
      }

      if (host.youtube.hostname.includes(hostnameFormBody)) {
        const simpleUrl = url.replace("watch?v=", "embed/")
        generatedEmbedCode = `<iframe width="1191" height="670" src="${simpleUrl}" frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
        store.set('embedded', generatedEmbedCode)
        embededContentJson = await youtubePuppeteer(urlPath)
        isRequestSuccess = true;
      }

      if (isRequestSuccess) {
        return res.json({
          status: "success",
          data: embededContentJson
        })
      } else {
        return res.json({
          status: 'error',
          message: 'Error in crawling hosts'
        })
      }
    }

    return res.render('index', {
      'iframe': store.get('embedded') || ""
    })
  },
}
