var urlParser = require('url');
const host = require('../../config/hosts');

const embedGenerator = (socialNetworkUrl) => {

  const hostnameFormBody = urlParser.parse(socialNetworkUrl).hostname

  let generatedEmbedCode = ""

  if (host.facebook.hostname.includes(hostnameFormBody)) {
    generatedEmbedCode = `<iframe
    src = "https://www.facebook.com/plugins/post.php?href=${socialNetworkUrl}&show_text=1&width=500"
    width="500" height="759" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"
    allow="encrypted-media"></iframe>`
  }

  if (host.pinterest.hostname.includes(hostnameFormBody)) {
    generatedEmbedCode = `<a data-pin-do="embedPin" href="${socialNetworkUrl}"></a>
    <script async defer src="https://assets.pinterest.com/js/pinit.js"></script>`
  }

  if (hostnameFormBody == host.telegram.hostname) {
    const simpleUrl = socialNetworkUrl.match(/https:\/\/t.me\/((\w+)\/(\d+))/)[1]
    generatedEmbedCode = `<script async src="https://telegram.org/js/telegram-widget.js?8" data-telegram-post="${simpleUrl}"
    data-width="100%" data-userpic="true"></script>`
  }

  if (host.youtube.hostname.includes(hostnameFormBody)) {
    const simpleUrl = socialNetworkUrl.replace("watch?v=", "embed/")
    generatedEmbedCode = `<iframe width="1191" height="670" src="${simpleUrl}" frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
  }
  return generatedEmbedCode
}

module.exports = embedGenerator
