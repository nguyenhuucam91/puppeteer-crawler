const puppeteer = require("puppeteer");

module.exports = async (url) => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process',
      '--proxy-server=http://10.3.51.70:6210'
    ]
  });
  const page = await browser.newPage();
  await page.goto(url, {
    waitUntil: 'load',
  });

  for (const frame of page.mainFrame().childFrames()) {
    const youtubeAvatar = await getYoutubeAvatar(frame)
    const youtubeUsername = await getYoutubeUser(frame)
    const youtubeVideoTitle = await getYoutubeVideoTitle(frame)
    const youtubeVideoThumbnail = await getYoutubeThumbnail(frame)
    const youtube = Object.assign(youtubeAvatar, youtubeUsername, youtubeVideoTitle, youtubeVideoThumbnail)
    return youtube
  }

  await browser.close();
}

async function getYoutubeAvatar(frame) {
  const selector = `a.ytp-title-channel-logo`
  return await frame.$eval(selector,
    element => {
      return {
        avatar: element.getAttribute('style').match(/background-image: url[(]"(.+)"[)]/)[1],
      }
    })
}

async function getYoutubeUser(frame) {
  const selector = `h2.ytp-title-expanded-title > a`
  return await frame.$eval(selector,
    element => {
      return {
        username: element.innerText,
      }
    })
}

async function getYoutubeVideoTitle(frame) {
  const selector = `a.ytp-title-link`
  return await frame.$eval(selector,
    element => {
      return {
        title: element.textContent,
      }
    })
}

async function getYoutubeThumbnail(frame) {
  const selector = `.ytp-cued-thumbnail-overlay-image`
  return await frame.$eval(selector,
    element => {
      return {
        thumbnail: element.getAttribute('style').match(/background-image: url[(]"(.+)"[)]/)[1],
      }
    })
}
