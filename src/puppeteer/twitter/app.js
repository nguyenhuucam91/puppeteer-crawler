const puppeteer = require("puppeteer");
const puppeteerConfig = require('../config/puppeteer')

const url = `file:///Volumes/Major/Practice/Crawler/socials/twitter.html`;

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process',
      '--proxy-server=http://10.3.51.70:6210'
    ]
  });
  const page = await browser.newPage();
  await page.goto(`${url}`, {
    waitUntil: 'networkidle0',
    timeout: 0
  });

  // await page.waitForFunction('document.querySelector("blockquote.twitter-tweet")')


  // let output = [];
  // console.log(await page.content())
  // const twitterText = await getText(page);

  for (const frameElement of page.mainFrame().childFrames()) {
    // console.log(1)
    const twitterContent = await getText(frameElement)
    console.log(twitterContent);
  }

  await browser.close();
})()

async function getText(frame) {

  const text = await frame.$eval('.EmbeddedTweet', element => {
    return element.outerHTML
  })
  // console.log(1)
  return text;
}
